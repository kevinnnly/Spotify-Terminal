const blessed = require('blessed');
let util = require('../widget-util');

let api = require("../auth-spotify").spotifyApi;

let playstatus = require('./status-bar/play-status');
let device = require('./status-bar/device');
let volume = require('./status-bar/volume');
let player = require('./status-bar/player');

class StatusBar {
    constructor() {
        this._buildWidget();
        this._keyEvents();
        util = new util(this.widget, 'status-bar');

        this.update();
        // setInterval(()=> this.update(), 60000);
        setInterval(()=> this.update(), 1000);
    }

    _buildWidget() {

        this.widget = blessed.box({
            bottom: 0,
            width: '100%',
            height: 4,
            border: {
                type: 'line'
            },
            style: {
                fg: 'white',
                border: {
                    fg: 'white'
                }
            },
            shrink: false
        })

        this.widget.append(player.widget);
        this.widget.append(playstatus.widget);
        this.widget.append(device.widget);
        this.widget.append(volume.widget);
    }

    _keyEvents() {

        this.widget.screen.key(['space'], () => {
            this.eventTogglePlay();
        });
        this.widget.screen.key(['n'], () => {
            this.eventNext();
        });
        this.widget.screen.key(['p'], () => {
            this.eventPrevious();
        });
    }

    async update() {

        let data = await api.getMyCurrentPlaybackState();
        data = data.body;

        this.current = data;

        playstatus.setData({
            song: data.item.name,
            artist: data.item.artists,
            progress: data.progress_ms,
            duration: data.item.duration_ms,
            playing: data.is_playing
        });
        player.setStatus({
            playing: data.is_playing,
            shuffle: data.shuffle_state,
            repeat: data.repeat_state
        });
        volume.setVolume(data.device.volume_percent);
        device.setDevice(data.device);

        util.render();
    }

    async eventTogglePlay() {
        if (player.state.playing) {
            await api.pause();
            // player.setStatus({ playing: false });
            // playstatus.setData({ playing: false });
            this.update();
        } else {
            await api.play();
            this.update();
            // player.setStatus({ playing: true });
            // playstatus.setData({ playing: true });
        }
        util.render();
    }

    async eventNext() {
        await api.skipToNext();
        await this.update();
    }

    async eventPrevious() {
        await api.skipToPrevious();
        await this.update();
    }

}

module.exports = new StatusBar();