const blessed = require('blessed');

let iconmap = {
    play: '▶',
    pause: '❚❚'
}

class player {

    constructor() {

        this.state = {
            playing: false,
            shuffle: false,
            repeat: false
        };

        this.widget = blessed.text({
            content: `${iconmap.play}`,
            shrink: true,
            left: 1,
            width: "shrink"
        })
    }

    setStatus(state) {
        Object.assign(this.state, state);
        this.update();
    }

    update() {
        this.widget.content = '';
        this.widget.content += this.state.playing ? iconmap.pause : iconmap.play;
    }
}

module.exports = new player();