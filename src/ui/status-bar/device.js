const blessed = require('blessed');

class Device {

    constructor() {
        this.widget = blessed.text({
            right: 0,
            bottom: 0,
        });
        this.device = {};
    }

    update() {
        this.widget.content = `♪Device:${this.device.name}`;
    }

    setDevice(device) {
        this.device = device;
        this.update();
    }

}

module.exports = new Device();