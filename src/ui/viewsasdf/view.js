
class View {
    constructor(title) {
        this.title = title || "";
        this._parent;
        this._child;
    }


    setItems(views) {

        views.map((view) => {
            this.widget.addItem(view.title);
        });
    }

    initActions() {
        this.widget.key('s', () => {
            
        });
    }

    get parent() { return this._parent }
    get child() { return this._child }

    set parent(x) { this._parent = x; }
    set child(x) { this._child = x; }
}

module.exports = View;