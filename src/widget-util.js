class WidgetUtils {
    constructor(widget, name) {
        this.widget = widget;
        this.name = name;

        this.debug = false;
    }

    error(message) {
        if (typeof message == "object") 
            message = JSON.stringify(message);
        this.widget.screen.log(`${this.name}:${message}`);
    }

    log(message) {

        if (typeof message == "object") 
            message = JSON.stringify(message);
        
        if (this.debug)
            this.widget.screen.log(`${this.name}:${message}`);
    }

    render() {
        this.widget.screen.render();
    }
}

module.exports = WidgetUtils;