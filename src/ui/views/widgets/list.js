const blessed = require('blessed');
class List {
    constructor() {
        this.widget = blessed.list({
            width: '100%',
            height: '100%',
            top: -1,
            left: -1,
            keys: true,
            vi: true,
            tags: true,
            // hidden : true, 
            items: [],
            border: {
                type: 'line'
            },
            style: {
                selected: {
                    fg: 'black',
                    bg: 'white'
                },
                fg: 'white',
                border: {
                    fg: 'white'
                }
            }
        });
    }

    /**
     * populates with a list of view objects
     * @param {View} views 
     */
    setItems(views) {
        views.map((view) => {
            this.widget.addItem(view.title);
            this.widget.items.slice(-1)[0]._data = view;
        });
    }
}

module.exports = List;