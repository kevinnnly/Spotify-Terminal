const blessed = require('blessed');

// let SongList = require('./views/songlist');
// let List = require('./views/list');

let util = require("../widget-util");
let viewUtil = require("./view-utils");
let api = require("../auth-spotify").spotifyApi;

let base = require('./presetViews/base');

class Viewer {
    constructor() {
        this.widget = blessed.box({
            width: '100%',
            top: 0,
            left: 0,
            border: {
                type: 'line'
            },
            style: {
                fg: 'white',
                border: {
                    fg: 'white'
                }
            }
        });

        viewUtil.main = this.widget;
        this.activePanels = viewUtil.activeViews;

        util = new util(this.widget, 'viewer');

        // preender
        this.widget.on('prerender', () => {
            if (this.widget && this.widget.height) {
                this.widget.height = this.widget.screen.height - 3;
                this.renderPanels();
            }
        });

        setTimeout(() => { this.initPanels(); }, 100);
    }

    initPanels() {
        viewUtil.current = base;
    }

    renderPanels() {

        if (this.activePanels.filter(Boolean).length == 0) {
            return;
        } else {
            let ratios = viewUtil.getRatios();
            let dimensions = this.calcDimensions(ratios)

            dimensions.map((dim, index) => {
                if (!dim) return;
                if (!this.activePanels[index].appended) return;
                this.activePanels[index].view.widget.width = dim.width;
                this.activePanels[index].view.widget.left = dim.left;
            });
        }

    }

    calcDimensions(ratios) {
        let left = 0;
        let maxWidth = this.widget.width;
        let results = [];

        ratios.map((ratio, index) => {

            if (!ratio)
                results.push(null);
            else {
                let width = Math.floor(maxWidth * ratio);
                left--;
                results.push({ width, left });
                left += width;
            }
        })

        let total = results.reduce((prev, curr) => { return (curr && curr.width) ? prev + curr.width : prev; }, 0);
        if (maxWidth - total + results.filter(Boolean).length - 1 > 0) {
            results[1].width += maxWidth - total + results.filter(Boolean).length - 1;
            results[2] && (results[2].left += maxWidth - total + results.filter(Boolean).length - 1);
        }
        return results;

    }


    async demo() {

        let asdf = await api.getPlaylist('spotify', '37i9dQZEVXcMwywtvDnvwd');

        let songs = asdf.body.tracks.items;

        this.songlist.songs = songs;
        this.songlist.displaySongs();

        this.songlist.widget.focus();

        // this.songlist.widget.on('keypress', function(ch, key) {
        //     util.error(key)
        // })


        this.songlist.widget.on('select', function () {

            let selected = this.items[this.selected];
            api.play({
                context_uri: 'spotify:user:spotify:playlist:37i9dQZEVXcMwywtvDnvwd',
                offset: {
                    uri: selected._data.track.uri
                }
            }).catch((err) => {
                console.log(err);
            })

        })


    }
}

module.exports = new Viewer();