const List = require('./list');

let playlists = require('./base/playlists');


class Base extends List {
    constructor(){
        super();

        let views = [playlists]
        this.setItems(views);
    }
}

module.exports = Base;