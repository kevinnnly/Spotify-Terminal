const blessed = require('blessed');

class Volume {
    
    constructor() {
        this.volume = 100;
        this.widget = blessed.text({
            right: 0,
            content : ''
        });
    }

    update() {
        this.widget.content = `Vol:${this.volume}`;
    }

    setVolume(vol) {
        this.volume = vol;
        this.update();
    }
}

module.exports = new Volume();